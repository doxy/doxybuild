Doxy Build
==========

This repo contains build scripts for the Doxy system. At present, only LaTeX
is supported, via latexmk.

Installation
------------

To install, run::

  pip install doxybuild

Maintainers
-----------

* `Alex Thorne <alex@thorne.cc>`_

Docker Image
------------

An Ubuntu based docker image is provided containing TeXLive, doxybuild itself
and some other useful tools. The following core LaTeX packages are installed:

* `texlive-full`
* `latexmk`
* `biber`

In addition, modern JavaScript document builder DiCy is included, along with
Node itself and the npm package manager. Other included packages are:

* `make`
* `git`
* `python3`
* `pip`
